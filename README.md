Project Title:
Dirt Rally Trio Edition

Project Description:
This is a three-player racing game that was implemented using networking in Java platform. Here we used TCP. The server initiates the game. He/she is responsible for picking the racing environment. The server is also the player which means that he/she gets to pick a car. The clients or players then just joins in the game that the server created. They can also pick any car of their choice. 

Prerequisites:
Eclipse 4.6.2
Java 1.8.0_162

Other packages:
Java FX for rendering

Project Directory:

-application
	-This is the code package where the sub menu for choosing the client and server takes place. .fxml files are also located here. Also the Go Back button returns here.


-environment
	-This is where the bounds and other properties of the environments are put. We have created three environments namely (1) Ancient Ruins, (2) Beach, (3) Jungle. If ever the car gets stuck near one of the boundaries, they move really slowly.

-menu
	-This is the Main Menu that gets introduced first to the user. Aside from playing the game by pressing Create Game, the user can also view what this game is about and who are the creators of this game.

-packets
	-The messages that are sent to and fro the server and the clients are classified into various types. It can range from a typical message, the player's details, and any updates.

-racing_game
	-This is the meat of the whole project. In this subfolder, the client, server, status updater,vector and car classes are well-defined. This is where 


Running this project:
Server
1. Go to package Menu.
2. Go to Test.java.
3. Run it.
4. Click New Game.
5. Click the button with plus sign.
Client
1. Go to package Menu.
2. Go to Test.java.
3. Run it.
4. Click New Game.
5. Click the button with people sign.

Notes:
-There are cases wherein at the first run the server and client does not work in a sense that one of the car is misplaced at the bottom of the screen. This is attributed to the Thread itself causing disruptions. Just rerun both it again.
-For any server or client errors, just rerun the whole thing.

Authors:
Patrick Bungabong
Shara Molina
April Joy Padrigano

3rd Year 2nd Semester [S.Y. 2016-2017]

Acknowledgement:
Thank you to Mr. Ryan Daga for his guidance all throughout the course to make this project possible.