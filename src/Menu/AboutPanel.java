package Menu;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;

public class AboutPanel extends JPanel{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Utilities util;
	private JButton backJB;
	private ImageIcon background;
	public ImageIcon backIn = new ImageIcon(getClass().getResource("/go_back_in.png"));
	public ImageIcon backOut = new ImageIcon(getClass().getResource("/go_back_out.png"));
	public AboutPanel(){
		setLayout(null);
		setOpaque(false);
		setBounds(0,0,1000,700);
		setPreferredSize(new Dimension(1000, 700));
		util = new Utilities();
		backJB = new JButton();
		util.buttonProperties(backJB, 10, 530, 250, 100, this);
		backJB.setIcon(backOut);
		backJB.setRolloverEnabled(true);
		backJB.setRolloverIcon(backIn);
	}
	public void paintComponent(Graphics g){
		background = new ImageIcon(getClass().getResource("/about.png"));
		background.paintIcon(this,g,0,0);
		//title = new ImageIcon(getClass().getResource("/title.png"));
		//title.paintIcon(this, g, 300, 120);
	}
	public void addListener(ActionListener listen){
		backJB.addActionListener(listen);
	}
	public JButton getBack(){
		return backJB;
	}
}
