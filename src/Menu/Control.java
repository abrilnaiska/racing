package Menu;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.MalformedURLException;

import javax.swing.JOptionPane;

import javafx.application.Application;
import application.Main;
public class Control {
	private View view;
	private MenuMe menuMe;
	private CreditsPanel creditsPanel;
	private AboutPanel aboutPanel;
	private VenuePanel venuePanel;
	String[] args = null;

	public void setArgs(String[] args){
		this.args = args;
	}
	public Control(View view){	
		this.view = view;
	}
	//firstcall this
	public void setAllPanels() throws MalformedURLException{
		//unite panels here with view panels
		setPanelsToView();
		//initialize button listener for all panels
		initPanelsButtons();
	}
	public void setPanelsToView(){
		this.menuMe = view.getMenuMe();
		this.creditsPanel = view.getCreditsPanel();
		this.aboutPanel = view.getAboutPanel();
		this.venuePanel = view.getVenuePanel();
	
	}
	public void initPanelsButtons(){
		this.menuMe.addListener(new ButtonListener());
		this.creditsPanel.addListener(new ButtonListener());
		this.aboutPanel.addListener(new ButtonListener());
		this.venuePanel.addListener(new ButtonListener());
	}
	class ButtonListener implements ActionListener{
		public void actionPerformed(ActionEvent event){
			try{
				if(event.getSource() == menuMe.getStartJB()){
					menuMe.getStartJB().setSelected(false);
					startGame();
				}else if(event.getSource() == menuMe.getAboutJB()){
					view.Switch("AboutPanel");
				}else if(event.getSource() == menuMe.getCreditsJB()){
					view.Switch("CreditsPanel");
				}else if(event.getSource() == aboutPanel.getBack()){
					view.Switch("MenuPanel");
				}else if(event.getSource() == creditsPanel.getBack()){
					view.Switch("MenuPanel");
				}
			}catch(Exception e){
				e.printStackTrace();
				JOptionPane.showMessageDialog(menuMe, "Sorry something went wrong.");
			}
		}	
	}
	public void startGame(){
		view.setVisible(false);
		Application.launch(Main.class, this.args);
	}
	
}
