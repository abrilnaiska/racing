package Menu;
/*This is for the MENU BUTTONS.*/
import java.awt.*;
import javax.swing.*;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;


public class MenuMe extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ImageIcon background;
	private ImageIcon title;
	private JButton startJB;
	private JButton aboutJB;
	private JButton creditsJB;
	public ImageIcon startOut = new ImageIcon(getClass().getResource("/new_game_in.png"));
	public ImageIcon aboutOut = new ImageIcon(getClass().getResource("/about_in.png"));
	public ImageIcon creditsOut = new ImageIcon(getClass().getResource("/creators_in.png"));
	public ImageIcon backOut = new ImageIcon(getClass().getResource("/go_back_in.png"));
	public ImageIcon startIn = new ImageIcon(getClass().getResource("/new_game_out.png"));
	public ImageIcon aboutIn = new ImageIcon(getClass().getResource("/about_out.png"));
	public ImageIcon creditsIn = new ImageIcon(getClass().getResource("/creators_out.png"));
	public ImageIcon backIn = new ImageIcon(getClass().getResource("/go_back_out.png"));
	private Utilities util;
	public MenuMe(){
		setLayout(null);
		setOpaque(false);
		setBounds(0,0,1000,700);
		setPreferredSize(new Dimension(1000, 700));
		util = new Utilities();
		startJB = new JButton();
		aboutJB = new JButton();
		creditsJB = new JButton();
		util.buttonProperties(startJB, 700, 120, 250, 100, this);
		util.buttonProperties(aboutJB, 700, 190, 250, 100, this);
		util.buttonProperties(creditsJB, 700, 260, 250, 100, this);
		startJB.setIcon(startOut);
		aboutJB.setIcon(aboutOut);
		creditsJB.setIcon(creditsOut);
		startJB.setRolloverEnabled(true);
		aboutJB.setRolloverEnabled(true);
		creditsJB.setRolloverEnabled(true);
		startJB.setRolloverIcon(startIn);
		aboutJB.setRolloverIcon(aboutIn);
		creditsJB.setRolloverIcon(creditsIn);
		
		
	}
	public void paintComponent(Graphics g){
		background = new ImageIcon(getClass().getResource("/main_menu.png"));
		background.paintIcon(this,g,0,0);
	}
	public void addListener(ActionListener listen){
		startJB.addActionListener(listen);
		aboutJB.addActionListener(listen);
		creditsJB.addActionListener(listen);
	}
	public void buttonAfterpress(){
		startJB.setIcon(startOut);
		aboutJB.setIcon(aboutOut);
		creditsJB.setIcon(creditsOut);
	}
	public void buttonSetSelected(boolean bol){
		startJB.setSelected(bol);
		aboutJB.setSelected(bol);
		creditsJB.setSelected(bol);
	}
	public JButton getStartJB(){ return startJB; }
	public JButton getAboutJB(){return aboutJB;}
	public JButton getCreditsJB(){return creditsJB;}
}
