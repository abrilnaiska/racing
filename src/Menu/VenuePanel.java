package Menu;

import java.awt.Dimension;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;

public class VenuePanel extends JPanel{
	private Utilities util;
	private JButton backJB;
	private JButton forestJB;
	private JButton ghostTownJB;
	public VenuePanel(){
		setLayout(null);
		setOpaque(false);
		setBounds(0,0,1000,700);
		setPreferredSize(new Dimension(1000, 700));
		util = new Utilities();
		backJB = new JButton("Back");
		util.buttonProperties(backJB, 0, 550, 150, 70, this);
		forestJB = new JButton("Forest");
		util.buttonProperties(forestJB, 350, 150, 150, 70, this);
		ghostTownJB = new JButton("Ghost Town");
		util.buttonProperties(ghostTownJB, 350, 300, 150, 70, this);
	}
	public void addListener(ActionListener listen){
		backJB.addActionListener(listen);
		forestJB.addActionListener(listen);
		ghostTownJB.addActionListener(listen);
	}
	public JButton getBack(){
		return backJB;
	}
	public JButton getForest(){
		return forestJB;
	}
	public JButton getGhostTown(){
		return ghostTownJB;
	}
	
}
