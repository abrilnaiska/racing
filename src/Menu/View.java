package Menu;
import java.awt.*;
import javax.swing.*;

public class View extends JFrame {
	
	private static final long serialVersionUID = 1L;
	private CardLayout layout;
	private JPanel panel;
	private MenuMe menuMe;
	private AboutPanel aboutPanel;
	private CreditsPanel creditsPanel;
	private VenuePanel venuePanel;
	public View(){
		
		super("Racing Game Master");
		setLayout(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(1000, 700);
		setLocationRelativeTo(null);
		setResizable(false);
		initializePanels();
		
		panelProperties(menuMe);
		layout = new CardLayout();
		panel = new JPanel();
		panel.setBounds(0,0,1000,700);
		panel.setLayout(layout);
		addToMainPanel();
		add(panel);
		setVisible(true);
	}
	public void initializePanels(){
		menuMe = new MenuMe();
		aboutPanel = new AboutPanel();
		creditsPanel = new CreditsPanel();
		venuePanel = new VenuePanel();
	}
	public void addToMainPanel(){
		panel.add(menuMe, "MenuPanel");
		panel.add(creditsPanel, "CreditsPanel");
		panel.add(aboutPanel, "AboutPanel");
		panel.add(venuePanel, "VenuePanel");
	}
	public void panelProperties(JPanel panelMe){
		panelMe.setBounds(0,0,1000,700);
		panelMe.setFocusable(false);
	}
	public void Switch(String str){
		layout.show(panel, str);
	}
	
	public MenuMe getMenuMe(){	return menuMe; }
	public AboutPanel getAboutPanel(){return aboutPanel;}
	public CreditsPanel getCreditsPanel(){return creditsPanel;}
	public VenuePanel getVenuePanel(){return venuePanel;}
}

