package Packets;

import java.io.Serializable;

public class Player implements Serializable{
	String name;
	int carNum;
	public Player(String t, int num){
		name = t;
		carNum = num;
	}
	
	public String getName(){
		return name;
	}
	
	public int getCarNum() {
		return carNum;
	}
	
}
