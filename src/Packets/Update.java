package Packets;

import java.io.Serializable;

public class Update implements Serializable {
	Double direction, x, y;
	String from;
	int lap;
	int lastLap;
	public Update(String a, double d, double xloc, double yloc, int lap, int lastLap){
		from = a;
		direction = d;
		x = xloc;
		y = yloc;
		this.lap = lap;
		this.lastLap = lastLap;
	}
	
	public Double getDirection(){
		return direction;
	}
	
	public Double getX(){
		return x;
	}
	
	public Double getY(){
		return y;
	}
	
	public String getFrom(){
		return from;
	}
	
	public int getLap() {
		return lap;
	}
	public int getLastLap() {
		return lastLap;
	}
}
