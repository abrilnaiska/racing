package application;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import environment.*;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import javafx.stage.Window;
import racing_game.Server;

public class createController implements Initializable {
	
	@FXML
    Button readybtn;
	@FXML
	TextField namefield;
	@FXML
	Label namelabel, hostlabel;
	@FXML
	ComboBox<String> combobox, combobox2;
	@FXML
	ImageView trackimage, image;
	
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		
		//set up combobox
		ObservableList<String> options = 
			    FXCollections.observableArrayList("Car 1","Car 2", "Car 3");
		combobox.setItems(options);
	    combobox.setValue("Car 1");
	    combobox.valueProperty().addListener(new ChangeListener<String>(){
	    	 @Override 
	    	 public void changed(ObservableValue ov, String t, String t1) {
	    		 if(t1.equals("Car 1"))
	    		 	trackimage.setImage(new Image("car1.png"));
	    		 else if(t1.equals("Car 2"))
		    		 	trackimage.setImage(new Image("car2.png"));
	    		 else
		    		trackimage.setImage(new Image("car3.png"));
	    	 }    
	    });
	    
	    ObservableList<String> bgoptions = 

			    FXCollections.observableArrayList("Jungle","Ancient Ruins","Beach");

			    FXCollections.observableArrayList("Jungle","Ancient Ruins","Oasis");

		combobox2.setItems(bgoptions);
	    combobox2.setValue("Jungle");
	   
	    combobox2.valueProperty().addListener(new ChangeListener<String>(){
	    	 @Override 
	    	 public void changed(ObservableValue ov, String t, String t1) {
	    		 if(t1.equals("Jungle"))
	    		 	image.setImage(new Image("bg1.png"));
	    		 else if(t1.equals("Ancient Ruins"))
	    			 image.setImage(new Image("bg2.png"));
	    		 else
		    		image.setImage(new Image("bg3.png"));
	    	 }    
	    });
	}
	
	public void backClicked(ActionEvent e) throws IOException{ 
	    Parent root = null;
	    Node source = (Node) e.getSource();
	    Stage stage = (Stage) source.getScene().getWindow();

	    
		root = FXMLLoader.load(getClass().getResource("MainMenu.fxml"));
		Scene scene = new Scene(root);
	    stage.setScene(scene);
	    stage.show();
	}
	
	public void readyClicked(ActionEvent e) throws IOException{ 
	    Parent root = null;
	    Node source = (Node) e.getSource();
	    Stage stage = (Stage) source.getScene().getWindow();

	    Server game = new Server();
	    game.playerName = namefield.getText();
	    game.carNum = Integer.parseInt(combobox.getValue().substring(combobox.getValue().length() - 1));
	    
	    if(combobox2.getValue().equals("Jungle")) {
	    	game.level = new Jungle();
	    	game.background = "0";
	    }
	    else if(combobox2.getValue().equals("Ancient Ruins")) {
	    	game.level = new AncientRuins();
	    	game.background = "1";
	    }
	    else if(combobox2.getValue().equals("Beach")) {
	    	game.level = new Beach();
	    	game.background = "2";
	    }

        try {
            game.start(stage);
            stage.centerOnScreen();
        }catch(Exception ex){}
	}

}
