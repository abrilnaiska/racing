package application;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import racing_game.Client;

public class joinController implements Initializable {

	
	@FXML
    Button joinbtn, backbtn;
	@FXML
	TextField namefield, addressfield;
	@FXML
	ComboBox<String> combobox;
	@FXML
	ImageView carimage;

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		ObservableList<String> options = 
			    FXCollections.observableArrayList("Car 1","Car 2", "Car 3");
		combobox.setItems(options);
	    combobox.setValue("Car 1");
	    combobox.valueProperty().addListener(new ChangeListener<String>(){
	    	 @Override 
	    	 public void changed(ObservableValue ov, String t, String t1) {
	    		 if(t1.equals("Car 1"))
	    		 	carimage.setImage(new Image("car1.png"));
	    		 else if(t1.equals("Car 2"))
		    		 	carimage.setImage(new Image("car2.png"));
	    		 else
		    		carimage.setImage(new Image("car3.png"));
	    	 }    
	    });
	}
	
	public void backClicked(ActionEvent e) throws IOException{ 
	    Parent root = null;
	    Node source = (Node) e.getSource();
	    Stage stage = (Stage) source.getScene().getWindow();

	    
		root = FXMLLoader.load(getClass().getResource("MainMenu.fxml"));
		Scene scene = new Scene(root);
	    stage.setScene(scene);
	    stage.show();
	}
	
	public void joinClicked(ActionEvent e) throws IOException{ 
	    Parent root = null;
	    Node source = (Node) e.getSource();
	    Stage stage = (Stage) source.getScene().getWindow();

	    Client game = new Client();
	    game.address = addressfield.getText();
	    game.playerName = namefield.getText();
	    game.carNum = Integer.parseInt(combobox.getValue().substring(combobox.getValue().length() - 1));
        System.out.println("CarNum: " + game.carNum);
	    try {
            game.start(stage);
            stage.centerOnScreen();
        }catch(Exception ex){}
	}

}
