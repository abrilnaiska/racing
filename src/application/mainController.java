package application;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

public class mainController implements Initializable {
	
	@FXML
    Button createbtn, joinbtn;
    
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {

	}
	
	public void createClicked(ActionEvent e) throws IOException{ 
	    Parent root;
	    Stage stage = (Stage) ((Node) e.getSource()).getScene().getWindow();
	    
		System.out.println("Create Clicked");
		root = FXMLLoader.load(getClass().getResource("createMenu.fxml"));
		
		Scene scene = new Scene(root);
		stage.setScene(scene);
	    stage.show();
	    
	}
	public void joinClicked(ActionEvent e) throws IOException{
		Parent root;
	    Stage stage = (Stage) ((Node) e.getSource()).getScene().getWindow();
	    
		System.out.println("Join Clicked");
		root = FXMLLoader.load(getClass().getResource("joinMenu.fxml"));
		
		Scene scene = new Scene(root);
		stage.setScene(scene);
	    stage.show();
	}

}
