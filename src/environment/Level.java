package com.racingGame.levels;

import java.util.ArrayList;

import javafx.scene.image.ImageView;
import javafx.scene.shape.Line;
import javafx.scene.shape.Polygon;

public interface Level {
	public double[] upperBounds();
	public double[] lowerBounds();
	public double[] checkPoints();
	public double[] checkPointsInv();
	
	public ArrayList<Line> checkLines();
	public ArrayList<Line> firstQtrLines();
	public ArrayList<Line> secondQtrLines();
	public ArrayList<Line> thirdQtrLines();
	public ArrayList<Line> fourthQtrLines();
	
	public Polygon firstPoly();
	public Polygon secondPoly();
	public Polygon thirdPoly();
	public Polygon fourthPoly();
	
	public double[] startX();
	public double[] startY();
	
	public ImageView levelGraphics();
}