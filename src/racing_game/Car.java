package racing_game;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.shape.Polyline;
import javafx.scene.transform.Rotate;

public class Car {

    double w = 35, h = 18;
    public DoubleProperty locationX, locationY;
    public double direction = 0;
    double speed = 0;
    ImageView graphicsImg;
    public String carName="";
    boolean powerUp = false;
    public int carNum;
    public int carLap = 4;
    public int lastLap =0;

    Polyline bounds = new Polyline(    	
    			34.0, 17.0, 34.0, 17.0, 
    			34.0, 17.0, 34.0, 17.0, 
    			34.0, 17.0, 34.0, 17.0, 
    			34.0, 17.0, 30.0, 17.0, 
    			30.0, 17.0, 9.0, 18.0, 
    			9.0, 18.0, 2.0, 18.0, 
    			2.0, 18.0, 2.0, 18.0, 
    			2.0, 18.0, 1.0, 2.0, 
    			2.0, 18.0, 1.0, 2.0, 
    			1.0, 2.0, 1.0, 2.0, 
    			1.0, 2.0, 23.0, 1.0, 
    			23.0, 1.0, 32.0, 1.0, 
    			23.0, 1.0, 32.0, 1.0
    			
    );
    
    Polyline front = new Polyline (
    		1.0, 10.0, 1.0, 5.0
    );

    public Car(String num, String name) {

    	graphicsImg = new ImageView(new Image(Server.class.getResourceAsStream("car"+num+".png")));
    	carName = name;
       
        locationX = new SimpleDoubleProperty(0);
        locationY = new SimpleDoubleProperty(0);
        
        graphicsImg.prefWidth(w);
        graphicsImg.prefHeight(h);
        
        graphicsImg.setRotationAxis(Rotate.Z_AXIS);
        graphicsImg.xProperty().bind(locationX.add(w / 2));
        graphicsImg.yProperty().bind(locationY.multiply(-1).add(Server.height - w / 2));
         
        bounds.translateXProperty().bind(graphicsImg.xProperty());
        bounds.translateYProperty().bind(graphicsImg.yProperty());
        bounds.rotateProperty().bind(graphicsImg.rotateProperty());
    }
    
    public void setCarImage(int num) {
    	carNum = num;
    	graphicsImg = new ImageView(new Image(Server.class.getResourceAsStream("car"+(num-1)+".png")));
    	locationX = new SimpleDoubleProperty(0);
        locationY = new SimpleDoubleProperty(0);
        
        graphicsImg.prefWidth(w);
        graphicsImg.prefHeight(h);
        
        graphicsImg.setRotationAxis(Rotate.Z_AXIS);
        graphicsImg.xProperty().bind(locationX.add(w / 2));
        graphicsImg.yProperty().bind(locationY.multiply(-1).add(Server.height - w / 2));
         
        bounds.translateXProperty().bind(graphicsImg.xProperty());
        bounds.translateYProperty().bind(graphicsImg.yProperty());
        bounds.rotateProperty().bind(graphicsImg.rotateProperty());
        
        front.translateXProperty().bind(graphicsImg.xProperty());
        front.translateYProperty().bind(graphicsImg.yProperty());
        front.rotateProperty().bind(graphicsImg.rotateProperty());
    }

    public void translateByVector(Vector v) {
        locationX.set(locationX.get() + v.getX());
        locationY.set(locationY.get() + v.getY());
    }

    public void setLocationByVector(double x, double y) {
        locationX.set(x);
        locationY.set(y);
    }

    public void setDirection(double angle) {
        graphicsImg.setRotate(180 - angle);
        direction = angle;
    }

    public void translateByRadius(double d) {
        Vector v = new Vector(d, direction / 180.0 * Math.PI, Vector.Polar);
        translateByVector(v);
    }

    public ImageView getGraphicsImg() {
        return graphicsImg;
    }
    
    public void setPowerUp(boolean powerUp) {
    	this.powerUp = powerUp;
    }
    
    public boolean getPowerUp() {
    	return powerUp;
    }
}
