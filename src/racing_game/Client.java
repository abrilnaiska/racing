package racing_game;

import Menu.Test;
import Packets.Message;
import Packets.Player;
import Packets.PlayerName;
import Packets.Update;
import environment.*;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.Path;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Polyline;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javafx.util.Duration;

public class Client extends Application {
	
	//UI
    public static double width = 800;
	public static double height = 600;
    Pane container;
    Button play; 
    
    //Game Elements
    double FPS = 30.0;
    Polyline linesUpper;
    Polyline linesLower;
    Timeline gameLoop;
    int laps = 3;
    
    //Client-Server
    public String address;
    ObjectOutputStream oos;
    ObjectInputStream ois;
    Double[]x={0.0,0.0,0.0},y={0.0,0.0,0.0},d={0.0,0.0,0.0};
    private ArrayList<Car> cars = new ArrayList<Car>();
    int count= 0;
    Socket link = null;
    int myNum;
    public int carNum;
    
    //Your Elements
    StatusUpdater statusUpdater = new StatusUpdater(width / 2 - 150, height / 2 - 35);
    ArrayList<Line> checkPoints = new ArrayList<>();
    ArrayList<Line> checkPointsInv = new ArrayList<>();
    int checks = 0;
    boolean keyPressed = false;
    KeyCode keyPressedCode = null;
    public String playerName, background;
    long time = 0;
    
    public Level level;
    Rectangle r;
    Text t;
    ProgressIndicator p;
    
    Label rank1, rank2, rank3;
    Button backbtn;
    @Override
    public void start(Stage primaryStage) throws Exception {
        initAll(primaryStage, 0);
    	container = new Pane();
   
        new Thread(new Runnable() {
        	@Override
			public void run() {
                try {
                	link = new Socket(address, 8888);
        			oos = new ObjectOutputStream(link.getOutputStream());
        			oos.writeObject(new PlayerName(playerName));
        			oos.writeObject(new Player(playerName, carNum));
                } catch (Exception e) {
                    System.out.println(e.toString());
                }
                new Thread(new Runnable() {
            		ObjectInputStream ois=null;
            		Object temp=null;
            		@Override
        			public void run() {
            			try {
            				temp=null;
							ois = new ObjectInputStream(link.getInputStream());
							do{
								temp = ois.readObject();
								if(temp.getClass().toString().equals("class Packets.PlayerName")){
									PlayerName p = (PlayerName) temp;
									cars.get(count++).carName = p.getName();
									System.out.println("NAme:"+ p.getName());
								} else if(temp.getClass().toString().equals("class Packets.Message")){
									Message m = (Message) temp;
									System.out.println("Message m: "+m.getMessage());
									if(m.getMessage().equals("StartGame")){
										Platform.runLater(new Runnable() {
				                            public void run() {
				                            	if(count==2)
				                            		cars.remove(count);
				                            	for(int x = 0; x<cars.size();x++){
				                                	cars.get(x).setLocationByVector(level.startX()[x],level.startY()[x]);
				                                	cars.get(x).setDirection(90);
				                                	System.out.println(x + " -> "+level.startX()[x] +" "+ level.startY()[x]);
				                                } 
				                            	for(int a = 0; a <cars.size(); a++)
				                            		container.getChildren().add(cars.get(a).getGraphicsImg());
				                            	
				                            	rank1.setText("1st: "+cars.get(0).carName);
			                        			rank2.setText("2nd: "+cars.get(1).carName);
			                        			container.getChildren().add(rank1);
			                        			container.getChildren().add(rank2);
			                        			if(cars.size() == 3) {
			                        				rank3.setText("1st: "+cars.get(2).carName);
			                        				container.getChildren().add(rank3);
			                        			}
				                            	
				                            	container.getChildren().removeAll(r,p,t);
				                                container.requestFocus();
				                                statusUpdater.setTextAndAnimate("Go!");
				                                time = System.currentTimeMillis();
				                            	gameLoop.play();
				                            	
				                            	
				                            }
										});
									} else if(m.getMessage().equals("0") || m.getMessage().equals("1") || m.getMessage().equals("2")){
										background = m.getMessage();
										System.out.println("background: "+background);
										Platform.runLater(new Runnable() {
											public void run() {
												initAll(primaryStage, Integer.parseInt(background));
											}
										});
									} else {
										statusUpdater.setText(m.getMessage());
									}
								} else if(temp.getClass().toString().equals("class Packets.Player")){
									Player p = (Player) temp;
									System.out.println("Player: "+p.getName()+"\ncar number: "+p.getCarNum()+"\n********");
									for(int a = 0; a < cars.size(); a++) {
										if(cars.get(a).carName.equals(p.getName())) {
											cars.get(a).setCarImage(p.getCarNum());
											break;
										}
									}
								}else if(temp.getClass().toString().equals("class Packets.Update")){
									Update u = (Update) temp;
									if(!u.getFrom().equals(playerName)){
										for(int a = 0; a < cars.size(); a++){
											if(u.getFrom().equals(cars.get(a).carName)){
												x[a] = u.getX();
												y[a] = u.getY();
												d[a] = u.getDirection();
												cars.get(a).carLap = u.getLap();
												cars.get(a).lastLap = u.getLastLap();
												//updateRanking();
											}
										}
									}
								}
							}while(true);
						} catch (IOException | ClassNotFoundException e) {
							try {
								ois.close();
								link.close();
							} catch (IOException e1) {
								e1.printStackTrace();
							}
						}
            		}
            	}).start();
                while(true){}
            }
        	
        }).start();
    	
        
    }

    private void loadLevel(double[] level_upper, double[] level_lower, ObservableList<Node> list) {

    	//draw Boundaries
    	linesUpper = new Polyline(level_upper);
        linesLower = new Polyline(level_lower);
        
        //load Background Image

        list.add(level.levelGraphics());
        
        //draw checkPoints
        checkPoints = new ArrayList<Line>();
        for (int i = 0; i < level.checkPoints().length; i += 4) {
            Line l = new Line(level.checkPoints()[i], level.checkPoints()[i + 1], level.checkPoints()[i + 2], level.checkPoints()[i + 3]);
            l.setStroke(Color.GREEN);
            l.setStrokeWidth(5);
            l.setOpacity(0.2);
            checkPoints.add(l);
            list.add(l);
        }
        
        checkPointsInv = new ArrayList<Line>();
        for(int i=0; i < level.checkPointsInv().length; i+=4) {
        	Line l = new Line(level.checkPointsInv()[i], level.checkPointsInv()[i + 1], level.checkPointsInv()[i + 2], level.checkPointsInv()[i + 3]);
        	l.setStroke(Color.RED);
            l.setStrokeWidth(5);
            l.setOpacity(0.2);
        	checkPointsInv.add(l);
        	//list.add(l);
        }
        
        //load statusUpdater
        statusUpdater = new StatusUpdater(width / 2 - 150, height / 2 - 35);
        list.addAll(statusUpdater);

    }

    private void checkForCollisions(Car car) {
        for (int i = 0; i < checkPoints.size(); i++) {
            Path p3 = (Path) Shape.intersect(car.bounds, checkPoints.get(i));
            if (!p3.getElements().isEmpty()) {
                if (checks == i && checks == 0) {
                    if (laps == 0) {
                        time = System.currentTimeMillis() - time;
                        statusUpdater.setText("Finished in:\n" + (int)((time / 1000.0) / 60.0) + " minutes\n"+ (time/1000)%60 + " seconds");
                        gameLoop.stop();
                        try {
							oos.writeObject(new Message(playerName+""));
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
                    } else {
                        statusUpdater.setTextAndAnimate("Laps Left\n    " + laps+" / 3");
                        laps--;
                        checks++;
                        car.carLap = laps;
                        for (int j = 1; j < checkPoints.size(); j++)
                            checkPoints.get(j).setStroke(Color.GREEN);
                        break;
                    }

                } else if (checks == i) {
                	checkPoints.get(i).setStroke(Color.RED);
                    statusUpdater.setTextAndAnimate("CheckPoint\n     " + checks+" / 4");
                    checks++;
                    break;
                }
                if (checks == checkPoints.size())
                    checks = 0;
            }
        }
        if ((PolylineIntersection(car.bounds, linesLower) || PolylineIntersection(car.bounds, linesUpper)))
            car.speed *= -0.5; 
        
        for(int f = 0 ;f<cars.size() ; f++){
        	if(f != myNum){
        		if(PolylineIntersection(cars.get(myNum).bounds,cars.get(f).bounds))
        		cars.get(myNum).speed *= -0.2;
        	}
        }
    }
    public void check(Car car) {
    	if(PolygonIntersection(car.front, level.fourthPoly())) {
    		for(int i=level.fourthQtrLines().size()-1; i>=0; i--) {
          		 Path p3 = (Path) Shape.intersect(car.front, level.fourthQtrLines().get(i));
                   if (!p3.getElements().isEmpty()) {
                  	 car.lastLap = i+300;
                  	 break;
                   }
       		}
    	} else if(PolygonIntersection(car.front, level.thirdPoly())) {
    		for(int i=level.thirdQtrLines().size()-1; i>=0; i--) {
          		 Path p3 = (Path) Shape.intersect(car.front, level.thirdQtrLines().get(i));
                   if (!p3.getElements().isEmpty()) {
                  	 car.lastLap = i+200;
                  	 break;
                   }
       		}
    	} else if(PolygonIntersection(car.front, level.secondPoly())) {
    		for(int i=level.secondQtrLines().size()-1; i>=0; i--) {
          		 Path p3 = (Path) Shape.intersect(car.front, level.secondQtrLines().get(i));
                   if (!p3.getElements().isEmpty()) {
                  	 car.lastLap = i+100;
                  	 break;
                   }
       		}
    	}else if(PolygonIntersection(car.front, level.firstPoly())) {
    		for(int i=level.firstQtrLines().size()-1; i>=0; i--) {
       		 Path p3 = (Path) Shape.intersect(car.front, level.firstQtrLines().get(i));
                if (!p3.getElements().isEmpty()) {
               	 car.lastLap = i;
               	 break;
                }
    		}
    		
    	}
    }
    public void updateRanking() {
    	ArrayList<Car> cs = new ArrayList<Car>();
    	for(int i=0; i<cars.size();i++) {
    		cs.add(cars.get(i));
    	}
    	sortByLastLap(cs);
    	sortByLap(cs);
    	String temp[] = new String[cs.size()];
    	for(int i=0; i<cs.size();i++) {
    		temp[i]=cs.get(i).carName+"";
    	}
    	if(cs.size()==2) {
    		rank1.setText("1st: "+ temp[0]+"");
    		rank2.setText("2nd: "+temp[1]+""); 
<<<<<<< HEAD:src/racing_game/Client.java
    		
=======
    		//exit and go back to main menu
			//Test test;
			//test = new Test();
			//System.exit(1);
>>>>>>> master:src/com/racingGame/Client.java
    	} else if(cs.size() == 3) {
    		rank1.setText("1st: "+ temp[0]+"");
    		rank2.setText("2nd: "+temp[1]+""); 
    		rank3.setText("3rd: "+temp[2]+"");
    		//exit and go back to main menu
			//Test test;
			//test = new Test();
			//System.exit(1);
    	}
    	
    }
    public void sortByLap(ArrayList<Car> cars) {
    	Collections.sort(cars, new Comparator<Car>() {
    		public int compare(Car c1, Car c2) {
    			Integer integer1, integer2;
    			integer1 = c1.carLap;
    			integer2 = c2.carLap;
    			return integer1.compareTo(integer2);
    		}
    	});
    }
    public void sortByLastLap(ArrayList<Car> cars) {
    	Collections.sort(cars, new Comparator<Car>() {
    		public int compare(Car c1, Car c2) {
    			Integer integer1, integer2;
    			integer1 = c1.lastLap;
    			integer2 = c2.lastLap;
    			return integer2.compareTo(integer1);
    		}
    	});
    }
    
    public static boolean PolylineIntersection(Polyline shape, Polyline world) {
        Path result = (Path) Shape.intersect(shape, world);
        return !result.getElements().isEmpty();
    }
    public static boolean PolygonIntersection(Polyline shape, Polygon world) {
    	Path result = (Path) Shape.intersect(shape, world);
    	return !result.getElements().isEmpty();
    }

    
    public void initAll(Stage primaryStage, int bg) {
    	container = new Pane();
        Scene scene = new Scene(container, width, height);
        primaryStage.setScene(scene);
        primaryStage.show();
        
        if(bg == 0)
        	level = new Jungle();
        else if(bg == 1)
        	level = new AncientRuins();
        else if(bg == 2) 
        	level = new Beach();
        
        //loading Background and Track Bounds
        loadLevel(level.lowerBounds(), level.upperBounds(), container.getChildren());
        
        cars = new ArrayList<Car>();
        cars.add(new Car("0",""));
        cars.add(new Car("1",""));
        cars.add(new Car("2",""));
        
        for(int x = 0; x<cars.size();x++){
        	cars.get(x).setLocationByVector(level.startX()[x],level.startY()[x]);
        	cars.get(x).setDirection(90);
        	System.out.println(x + " -> "+level.startX()[x] +" "+ level.startY()[x]);
        }
        
        container.setOnKeyPressed(e -> {
                keyPressed = true;
                keyPressedCode = e.getCode();
            });
        container.setOnKeyReleased(e -> {
                keyPressed = false;
        });

        gameLoop = new Timeline(new KeyFrame(Duration.millis(1000 / FPS), new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e) {

            	for(int a = 0; a < cars.size(); a++)
					if(playerName.equals(cars.get(a).carName))
						myNum = a;
            	
            	if (keyPressed) {
                    if (cars.get(myNum).speed != 0)
                        if (keyPressedCode == KeyCode.LEFT) 
                        	cars.get(myNum).setDirection(cars.get(myNum).direction += (3));
                        else if (keyPressedCode == KeyCode.RIGHT)
                        	cars.get(myNum).setDirection(cars.get(myNum).direction -= (3));

                    if (keyPressedCode == KeyCode.UP)
                    	cars.get(myNum).speed += 0.06;
                    else if (keyPressedCode == KeyCode.DOWN)
                    	cars.get(myNum).speed -= 0.06;
                }
                else
            		if(cars.get(myNum).speed > 0)
            			cars.get(myNum).speed-= 0.02;
            		else if(cars.get(myNum).speed < 0 )
            			cars.get(myNum).speed+= 0.02;
            	
            	cars.get(myNum).translateByRadius(cars.get(myNum).speed);
            	checkForCollisions(cars.get(myNum));
            	check(cars.get(myNum));
            	updateRanking();
            	
            	try {
					oos.writeObject(new Update(cars.get(myNum).carName,cars.get(myNum).direction,cars.get(myNum).locationX.get(),cars.get(myNum).locationY.get(), cars.get(myNum).carLap, cars.get(myNum).lastLap));
				} catch (IOException e1) {
						gameLoop.stop();
						container. getChildren().removeAll(backbtn);
						
						r = new Rectangle(width, height);
				        r.setFill(Color.WHITE);
				        r.setOpacity(0.6);
				        
				        t = new Text("Server error");
				        t.setX(20);
				        t.setY(height - 100);
				        t.setFont(Font.font(Font.getDefault().getName(), FontWeight.BOLD, 30));
				        t.setFill(new Color(107 / 255.0, 162 / 255.0, 252 / 255.0, 1.0));
				        
				        
						container.getChildren().addAll(r,t,backbtn);
				}
            	
            	for(int a = 0; a < cars.size(); a++){
            		if(!(cars.get(a).carName.equals(playerName))){
            			cars.get(a).setDirection(d[a]);
	            		cars.get(a).locationX.set(x[a]);
	            		cars.get(a).locationY.set(y[a]);
            		}
            	}
            }
        }));
        gameLoop.setCycleCount(Timeline.INDEFINITE);     
        r = new Rectangle(width, height);
        r.setFill(Color.WHITE);
        r.setOpacity(0.6);
        container.getChildren().addAll(r);
        t = new Text("Waiting for the Server");
        p = new ProgressIndicator();

        p.setLayoutX(width / 2 + 60);
        p.setLayoutY(height / 2 - 33);
        t.setX(width / 2 - 160);
        t.setY(height / 2);
        t.setFont(Font.font(Font.getDefault().getName(), FontWeight.BOLD, 17));
        t.setFill(new Color(107 / 255.0, 162 / 255.0, 252 / 255.0, 1.0));
        container.getChildren().addAll(t, p);
        
        Image imgback = new Image(getClass().getResourceAsStream("/back_mini.png"));
        backbtn = new Button("", new ImageView(imgback));
		backbtn.setStyle("-fx-background-color: transparent; -fx-outline: none; -fx-background-radius: 1000pt;");
		backbtn.setOnMouseEntered(e -> backbtn.setStyle("-fx-background-color: gray;"));
        backbtn.setOnMouseExited(e -> backbtn.setStyle("-fx-background-color: transparent; -fx-outline: none;"));
		backbtn.setLayoutX(20);
		backbtn.setLayoutY(height-90);
		
		container.getChildren().add(backbtn);
		
		backbtn.setOnAction(x -> {
			Parent root;
    	    Stage stage = (Stage) ((Node) x.getSource()).getScene().getWindow();
    	    
    	    try {
				root = FXMLLoader.load(getClass().getClassLoader().getResource("application/MainMenu.fxml"));
				Scene scene2 = new Scene(root);
        		stage.setScene(scene2);
        	    stage.show();
			} catch (IOException e3) {
				e3.printStackTrace();
			}
		});
		
        rank1 = new Label("1st: ");
		rank1.setFont(Font.font(Font.getDefault().getName(), FontWeight.EXTRA_BOLD, 30));
        rank1.setTextFill(new Color(226.0 / 255.0, 75.0 / 255.0, 61.0 / 255.0, 1.0));
		//rank1.setTextFill(Color.web("#ffffff"));
		rank1.setLayoutX(20);
		rank1.setLayoutY(20);
		rank1.setPrefSize(200, 30);
		rank2 = new Label("2nd: ");
		rank2.setFont(Font.font(Font.getDefault().getName(), FontWeight.EXTRA_BOLD, 25));
        rank2.setTextFill(new Color(103.0 / 255.0, 216.0 / 255.0, 93.0 / 255.0, 1.0));
		rank2.setLayoutX(20);
		rank2.setLayoutY(50);
		rank2.setPrefSize(200, 25);
		rank3 = new Label("3rd: ");
		rank3.setFont(Font.font(Font.getDefault().getName(), FontWeight.EXTRA_BOLD, 20));
        rank3.setTextFill(new Color(61.0 / 255.0, 106.0 / 255.0, 229.0 / 255.0, 1.0));
		rank3.setLayoutX(20);
		rank3.setLayoutY(70);
		rank3.setPrefSize(200, 20);
        
      //exit
        primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            public void handle(WindowEvent e) {
                System.exit(1);
            }
        });
    }

}
