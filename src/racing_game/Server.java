package racing_game;
import Menu.Control;
import Menu.Test;
import Menu.View;
import Packets.Message;
import Packets.Player;
import Packets.PlayerName;
import Packets.Update;
import environment.Level;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.Path;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Polyline;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javafx.util.Duration;

public class Server extends Application {
	
	//UI
    public static double width = 800;
	public static double height = 600;
    Pane container;
    
    //Game Elements
    double FPS = 30.0;
    Polyline linesUpper;
    Polyline linesLower;
    Timeline gameLoop;
    int laps = 3;
    
    //Client-Server
    String address;
    boolean server;
    ObjectOutputStream oos;
    ObjectInputStream ois;
    private ArrayList<ObjectOutputStream> outputs = new ArrayList<ObjectOutputStream>();
    private ArrayList<Car> cars = new ArrayList<Car>();
    Double[]x={0.0,0.0,0.0},y={0.0,0.0,0.0},d={0.0,0.0,0.0};
    int count=0;
    
    //Your Elements
    StatusUpdater statusUpdater = new StatusUpdater(width / 2 - 150, height / 2 - 35);
    ArrayList<Line> checkPoints = new ArrayList<>();
    int checks = 0;
    boolean keyPressed = false;
    KeyCode keyPressedCode = null;
    public String playerName, background;
    long time = 0;

    public int carNum;
    public Level level;

    ArrayList<String> finNames = new ArrayList<String>();
    ArrayList<String> finPlaces = new ArrayList<String>();
    int finished =0;
    
    Label rank1, rank2, rank3;
    String rankNames[];
    Button backbtn;
    @Override
    public void start(Stage primaryStage) throws Exception {
        
    	container = new Pane();
        Scene scene = new Scene(container, width, height);
        primaryStage.setScene(scene);
        primaryStage.show();
        
        //loading Background and Track Bounds
        loadLevel(level.lowerBounds(), level.upperBounds(), container.getChildren());
        
        //cars.add(new Car((carNum-1)+"",playerName));
        cars = new ArrayList<Car>();

        cars.add(new Car("0", playerName));
        cars.add(new Car("1",""));
        cars.add(new Car("2",""));
        
        for(int x = 0; x<cars.size();x++){
        	cars.get(x).setLocationByVector(level.startX()[x],level.startY()[x]);
        	cars.get(x).setDirection(90);
        	System.out.println(x + " -> "+level.startX()[x] +" "+ level.startY()[x]);
        }
        
        container.setOnKeyPressed(e -> {
                keyPressed = true;
                keyPressedCode = e.getCode();
            });
        container.setOnKeyReleased(e -> {
                keyPressed = false;
        });

        gameLoop = new Timeline(new KeyFrame(Duration.millis(1000 / FPS), new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e) {
            	if (keyPressed) {
                    if (cars.get(0).speed != 0)
                        if (keyPressedCode == KeyCode.LEFT) 
                        	cars.get(0).setDirection(cars.get(0).direction += (3));
                        else if (keyPressedCode == KeyCode.RIGHT)
                        	cars.get(0).setDirection(cars.get(0).direction -= (3));

                    if (keyPressedCode == KeyCode.UP)
                    	cars.get(0).speed += 0.06;
                    else if (keyPressedCode == KeyCode.DOWN)
                    	cars.get(0).speed -= 0.06;
                }
                else
            		if(cars.get(0).speed > 0)
            			cars.get(0).speed-= 0.02;
            		else if(cars.get(0).speed < 0 )
            			cars.get(0).speed+= 0.02;

            	
            	cars.get(0).translateByRadius(cars.get(0).speed);
            	checkForCollisions(cars.get(0));
            	check(cars.get(0));
            	updateRanking();
            	
            	for(int f = 0 ; f<outputs.size();f++){
            		for(int d = 0 ; d <cars.size() ; d++){
	            		try {
	    					outputs.get(f).writeObject(new Update(cars.get(d).carName,cars.get(d).direction,cars.get(d).locationX.get(),cars.get(d).locationY.get(), cars.get(d).carLap, cars.get(d).lastLap));
	            		} catch (IOException e1) {e1.printStackTrace();}
            		}
            	}
            	
            	for(int a = 1; a < cars.size(); a++){
        			cars.get(a).setDirection(d[a]);
            		cars.get(a).locationX.set(x[a]);
            		cars.get(a).locationY.set(y[a]);
            	}
            }
        }));
        gameLoop.setCycleCount(Timeline.INDEFINITE);
        gameLoop.play();
        
        
        Rectangle r = new Rectangle(width, height);
        r.setFill(Color.WHITE);
        r.setOpacity(0.6);
        container.getChildren().addAll(r);
        Text t = new Text("Waiting for Other Players..");
        ProgressIndicator p = new ProgressIndicator();

        p.setLayoutX(width / 2 + 50);
        p.setLayoutY(height / 2 - 33);
        t.setX(width / 2 - 200);
        t.setY(height / 2);
        t.setFont(Font.font(Font.getDefault().getName(), FontWeight.BOLD, 17));
        t.setFill(new Color(107 / 255.0, 162 / 255.0, 252 / 255.0, 1.0));
        container.getChildren().addAll(t, p);
        
        Button playbtn = new Button("Start the Game");
		playbtn.setStyle("-fx-font: 30 System;");
		playbtn.setPrefWidth(300);
		playbtn.setPrefHeight(50);
		playbtn.setLayoutX(width / 2 - 150);
		playbtn.setLayoutY(height / 2 - 37);
		
		Image imgback = new Image(getClass().getResourceAsStream("/back_mini.png"));
		backbtn = new Button("", new ImageView(imgback));
		backbtn.setStyle("-fx-background-color: transparent; -fx-outline: none; -fx-background-radius: 1000pt;");
		backbtn.setOnMouseEntered(e -> backbtn.setStyle("-fx-background-color: gray;"));
        backbtn.setOnMouseExited(e -> backbtn.setStyle("-fx-background-color: transparent; -fx-outline: none;"));
		backbtn.setLayoutX(20);
		backbtn.setLayoutY(height-90);
		
		rank1 = new Label("1st: ");
		rank1.setFont(Font.font(Font.getDefault().getName(), FontWeight.BOLD, 30));
        rank1.setTextFill(new Color(226.0 / 255.0, 75.0 / 255.0, 61.0 / 255.0, 1.0));
		rank1.setLayoutX(20);
		rank1.setLayoutY(20);
		rank1.setPrefSize(200, 30);
		rank2 = new Label("2nd: ");
		rank2.setFont(Font.font(Font.getDefault().getName(), FontWeight.BOLD, 25));
        rank2.setTextFill(new Color(103.0 / 255.0, 216.0 / 255.0, 93.0 / 255.0, 1.0));
		rank2.setLayoutX(20);
		rank2.setLayoutY(50);
		rank2.setPrefSize(200, 25);
		rank3 = new Label("3rd: ");
		rank3.setFont(Font.font(Font.getDefault().getName(), FontWeight.BOLD, 20));
        rank3.setTextFill(new Color(61.0 / 255.0, 106.0 / 255.0, 229.0 / 255.0, 1.0));
		rank3.setLayoutX(20);
		rank3.setLayoutY(70);
		rank3.setPrefSize(200, 20);
		
        new Thread(new Runnable() {
        	Socket socket= null;
        	
        	@Override
			public void run() {
                try {
                    while(count++<2){
                    	ServerSocket serverSocket = new ServerSocket(8888);
                    	
                    	socket = serverSocket.accept();
                    	oos = new ObjectOutputStream(socket.getOutputStream());
                    	outputs.add(oos);
                    	oos.writeObject(new Message(background+""));
                    	new Thread(new Runnable() {
                    		ObjectInputStream ois=null;
                    		Object temp=null;
                    		int c = count;
                    		@Override
							public void run() {
                    			try {
									ois = new ObjectInputStream(socket.getInputStream());
									do{
										temp = ois.readObject();
										if(temp.getClass().toString().equals("class Packets.PlayerName")){
											PlayerName p = (PlayerName) temp;
											cars.get(c).carName = p.getName();
										}else if(temp.getClass().toString().equals("class Packets.Update")){
											Update u = (Update) temp;	
											for(int a = 0; a < cars.size(); a++){
												if(u.getFrom().equals(cars.get(a).carName)){
													x[a] = u.getX();
													y[a] = u.getY();
													d[a] = u.getDirection();
													cars.get(a).carLap = u.getLap();
													cars.get(a).lastLap = u.getLastLap();
													
												}
											}
											
										} else if(temp.getClass().toString().equals("class Packets.Player")){
											Player p = (Player) temp;
											for(int a = 0; a < cars.size(); a++) {
												if(cars.get(a).carName.equals(p.getName())) {
													cars.get(a).setCarImage(p.getCarNum());
													break;
												}
											}
										} else if(temp.getClass().toString().equals("class Packets.Message")) {
											Message m = (Message) temp;
											finNames.add(m.getMessage() + "");	
											String list = "";
											if(outputs.size() == 1 && finNames.size() == 2) {
												list = "1st: " + finNames.get(0) +"\n"
														+ "2nd: " + finNames.get(1);
												
												statusUpdater.setText(list+"");
												for(int ctr = 0; ctr < outputs.size(); ctr++) {
													outputs.get(ctr).writeObject(new Message(list+""));
												}
												//exit and go back to main menu
												//Test test;
												//test = new Test();
												//System.exit(1);
											} else if(finNames.size() == 3) {
												list = "1st: " + finNames.get(0) +"\n"
														+ "2nd: " + finNames.get(1) +"\n"
														+ "3rd: " + finNames.get(2);
												
												statusUpdater.setText(list+"");
												for(int ctr = 0; ctr < outputs.size(); ctr++) {
													outputs.get(ctr).writeObject(new Message(list+""));
												}
<<<<<<< HEAD:src/racing_game/Server.java
=======
												//exit and go back to main menu
												//Test test;
												//test = new Test();
												//System.exit(1);
>>>>>>> master:src/com/racingGame/Server.java
											}
										}
									}while(true);
								} catch (IOException | ClassNotFoundException e) {
									try {
										ois.close();
									} catch (IOException e1) {
										e1.printStackTrace();
									}
								}	
							}	
                    	}).start();
                    	if(count == 1){
                    		Platform.runLater(new Runnable() {
	                            public void run() {	                            	
	                            	container.getChildren().removeAll(p,t);
	                            	container.getChildren().add(playbtn);
	                            	backbtn.setOnAction(e -> {
	                        			gameLoop.stop();
	                        			
                            			try {
                            				for(ObjectOutputStream b:outputs)
                            					b.close();
                            				
                            				serverSocket.close();
	                            	    	socket.close();
	                            	    	count = 0;
	                            	    	cars.clear();
	                            	    	
                            				Parent root;
    	                            	    Stage stage = (Stage) ((Node) e.getSource()).getScene().getWindow();
    	                            	    
											root = FXMLLoader.load(getClass().getClassLoader().getResource("application/MainMenu.fxml"));
											Scene scene = new Scene(root);
		                            		stage.setScene(scene);
		                            	    stage.show();
                        				} catch (IOException e1) {e1.printStackTrace();}
	                            	});
	                            	playbtn.setOnAction(e -> {
	                                	container.getChildren().removeAll(playbtn, r);
	                                	container.getChildren().add(backbtn);
	                                	
	                                	cars.get(0).setCarImage(carNum);
	                                    for(int a = 0; a <cars.size(); a++)
	                                		if(cars.get(a).carName.equals("")) 
	                                			cars.remove(a);
	        
	                        			for(int s = 0 ; s<outputs.size();s++){
	                        				for(int d = 0; d < cars.size();d++){
	    										try {
	    											outputs.get(s).writeObject(new PlayerName(cars.get(d).carName));
	    										} catch (IOException e1) {e1.printStackTrace();}
	                        				}
	                        			}
	                        			for(int x = 0; x<cars.size();x++){
	                        	        	cars.get(x).setLocationByVector(level.startX()[x],level.startY()[x]);
	                        	        	cars.get(x).setDirection(90);
	                        	        	System.out.println(x + " -> "+level.startX()[x] +" "+ level.startY()[x]);
	                        	        	
	                        	        }
	                        			
	                        			for(int s = 0 ; s<outputs.size();s++){
	                        				for(int d = 0; d < cars.size();d++){
	    										try {
	    											//outputs.get(s).writeObject(new PlayerName(cars.get(d).carName));
	    											outputs.get(s).writeObject(new Player(cars.get(d).carName, cars.get(d).carNum));
	    											
	    											
	    										} catch (IOException e1) {e1.printStackTrace();}
	                        				}
	                        			}
	                        			rankNames = new String[cars.size()];
	                        			for(int x=0; x < cars.size(); x++) {
	                        				container.getChildren().add(cars.get(x).getGraphicsImg());
	                        				rankNames[x] = cars.get(x).carName+"";
	                        			}
	                        			rank1.setText("1st: "+rankNames[0]);
	                        			rank2.setText("2nd: "+rankNames[1]);
	                        			container.getChildren().add(rank1);
	                        			container.getChildren().add(rank2);
	                        			if(cars.size() == 3) {
	                        				rank3.setText("3rd: "+rankNames[2]);
	                        				container.getChildren().add(rank3);
	                        			}
	                        			
	                        			container.requestFocus();
	                        			time = System.currentTimeMillis();
	                        			for(int d = 0; d < outputs.size();d++){
	    									try {
	    										outputs.get(d).writeObject(new Message("StartGame"));
	    									} catch (IOException e1) {e1.printStackTrace();}
	                    				}
	                    			
	                        		});
	                            }
							});
                    	}
                    	 //serverSocket.close();
                    }
                   
                } catch (Exception e) {
                    System.out.println("Server error " + e.toString());
                }
            }
        }).start();
        
        //exit
        primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            public void handle(WindowEvent e) {
                System.exit(1);
            }
        });
    }

    private void loadLevel(double[] level_upper, double[] level_lower, ObservableList<Node> list) {

    	//load Boundaries
    	linesUpper = new Polyline(level_upper);
        linesLower = new Polyline(level_lower);
       
        //load Background Image
        list.add(level.levelGraphics());
        
        //load checkPoints
        checkPoints = new ArrayList<Line>();
        for (int i = 0; i < level.checkPoints().length; i += 4) {
            Line l = new Line(level.checkPoints()[i], level.checkPoints()[i + 1], level.checkPoints()[i + 2], level.checkPoints()[i + 3]);
            l.setStroke(Color.GREEN);
            l.setStrokeWidth(5);
            l.setOpacity(0.2);
            checkPoints.add(l);
            list.add(l);
        }
        //load statusUpdater
        list.addAll(statusUpdater);
    }
    private void checkForCollisions(Car car) {
    	if ((PolylineIntersection(car.bounds, linesLower) || PolylineIntersection(car.bounds, linesUpper)))
            car.speed = 0; 
        
        for(int f = 0 ;f<cars.size() ; f++){
        	if(f != 0){
        		if(PolylineIntersection(cars.get(0).bounds,cars.get(f).bounds))
        			cars.get(0).speed = 0;
        	}
        }
        for (int i = 0; i < checkPoints.size(); i++) {
            Path p3 = (Path) Shape.intersect(car.bounds, checkPoints.get(i));
            if (!p3.getElements().isEmpty()) {
                if (checks == i && checks == 0) {
                    if (laps == 0) {
                        time = System.currentTimeMillis() - time;
                        statusUpdater.setText("Finished in:\n" + (int)((time / 1000.0) / 60.0) + " minutes\n"+ (time/1000)%60 + " seconds");          
                        gameLoop.stop();
                        finNames.add(playerName +"");
                       
                    } else {
                    	statusUpdater.setTextAndAnimate("Laps Left\n    " + laps+" / 3");
                        laps--;
                        checks++;
                        car.carLap = laps;
                        for (int j = 1; j < checkPoints.size(); j++)
                            checkPoints.get(j).setStroke(Color.GREEN);
                        break;
                    }

                } else if (checks == i) {
                    checkPoints.get(i).setStroke(Color.RED);
                    statusUpdater.setTextAndAnimate("CheckPoint\n     " + checks+" / 4");
                    checks++;
                    break;
                }
                if (checks == checkPoints.size())
                    checks = 0;
            }
        }
    }
    public void check(Car car) {
    	if(PolygonIntersection(car.front, level.fourthPoly())) {
    		for(int i=level.fourthQtrLines().size()-1; i>=0; i--) {
          		 Path p3 = (Path) Shape.intersect(car.front, level.fourthQtrLines().get(i));
                   if (!p3.getElements().isEmpty()) {
                  	 car.lastLap = i+300;
                  	 break;
                   }
       		}
    	} else if(PolygonIntersection(car.front, level.thirdPoly())) {
    		for(int i=level.thirdQtrLines().size()-1; i>=0; i--) {
          		 Path p3 = (Path) Shape.intersect(car.front, level.thirdQtrLines().get(i));
                   if (!p3.getElements().isEmpty()) {
                  	 car.lastLap = i+200;
                  	 break;
                   }
       		}
    	} else if(PolygonIntersection(car.front, level.secondPoly())) {
    		for(int i=level.secondQtrLines().size()-1; i>=0; i--) {
          		 Path p3 = (Path) Shape.intersect(car.front, level.secondQtrLines().get(i));
                   if (!p3.getElements().isEmpty()) {
                  	 car.lastLap = i+100;
                  	 break;
                   }
       		}
    	}else if(PolygonIntersection(car.front, level.firstPoly())) {
    		for(int i=level.firstQtrLines().size()-1; i>=0; i--) {
       		 Path p3 = (Path) Shape.intersect(car.front, level.firstQtrLines().get(i));
                if (!p3.getElements().isEmpty()) {
               	 car.lastLap = i;
               	 break;
                }
    		}
    		
    	}
    }
    public void updateRanking() {
    	ArrayList<Car> cs = new ArrayList<Car>();
    	for(int i=0; i<cars.size();i++) {
    		cs.add(cars.get(i));
    	}
    	sortByLastLap(cs);
    	sortByLap(cs);
    	String temp[] = new String[cs.size()];
    	for(int i=0; i<cs.size();i++) {
    		temp[i]=cs.get(i).carName+"";
    	}
    	if(cs.size()==2) {
    		rank1.setText("1st: "+ temp[0]+"");
    		rank2.setText("2nd: "+temp[1]+""); 
    	} else if(cs.size() == 3) {
    		rank1.setText("1st: "+ temp[0]+"");
    		rank2.setText("2nd: "+temp[1]+""); 
    		rank3.setText("3rd: "+temp[2]+"");
    	}
    	
    }
    public void sortByLap(ArrayList<Car> cars) {
    	Collections.sort(cars, new Comparator<Car>() {
    		public int compare(Car c1, Car c2) {
    			Integer integer1, integer2;
    			integer1 = c1.carLap;
    			integer2 = c2.carLap;
    			return integer1.compareTo(integer2);
    		}
    	});
    }
    public void sortByLastLap(ArrayList<Car> cars) {
    	Collections.sort(cars, new Comparator<Car>() {
    		public int compare(Car c1, Car c2) {
    			Integer integer1, integer2;
    			integer1 = c1.lastLap;
    			integer2 = c2.lastLap;
    			return integer2.compareTo(integer1);
    		}
    	});
    }
    public static boolean PolylineIntersection(Polyline shape, Polyline world) {
        Path result = (Path) Shape.intersect(shape, world);
        return !result.getElements().isEmpty();
    }
    public static boolean PolygonIntersection(Polyline shape, Polygon world) {
    	Path result = (Path) Shape.intersect(shape, world);
    	return !result.getElements().isEmpty();
    }
}
